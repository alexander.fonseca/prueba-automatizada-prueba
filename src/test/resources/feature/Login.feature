Feature: Login

  @lLoginSuccessful
  Scenario Outline: Login
    Given El cliente abre la aplicacion web
    When El cliente <usuario> y <contrasena>
    Then debo ver mi cuenta <validtexto>
    Examples:
      | usuario | contrasena | validtext                             |
      | test01  | 123Test+   | Your nearest branch closes in: 30m 5s |